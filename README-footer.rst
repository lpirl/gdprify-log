integration with logrotate
--------------------------

If you want to anonymize your logs when rotating them with `logrotate
<https://github.com/logrotate/logrotate>`__, place the following lines
in, e.g., ``/etc/logrotate.d/000-anonymize-before-compression``::

  compress
  compresscmd /bin/sh
  compressoptions -c '/where/ever/gdprify-log | gzip'

With this global setting, all logs are anonymized before compression
and it is not required to modify every logrotate rule.

credits
-------

The implementation to anonymize the rightmost bits of IP addresses has
been taken from https://www.privacyfoundation.ch/de/service/anonip.html
mainly. Big thanks.
