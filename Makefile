# disable automatic make rules
.SUFFIXES:

SCRIPT ?= ./gdprify-log
BENCHMARK_LOG ?= benchmark.log

INTERPRETERS ?= python3 pypy3
BENCHMARKS ?= $(INTERPRETERS:%=benchmark.%)
PROFILES ?= $(INTERPRETERS:%=profile.%)

README.rst: $(SCRIPT) README-header.rst README-footer.rst
	echo > $@
	cat README-header.rst >> $@
	echo >> $@
	$(SCRIPT) -h | sed 's/^/  /' >> $@
	echo >> $@
	cat README-footer.rst >> $@

smoke_test:
	echo -e 'foo bar@example.com\nbaz' \
	| ./gdprify-log \
	| grep -q 'foo b...@...e.com'

	echo -e 'foo\nbar http://198.78.56.2:8080/foo baz\nboo' \
	| ./gdprify-log \
	| grep -q 'bar http://198.78.56.0:8080/foo baz'

	echo -e 'foo\nbar host=2001:db8::ff00:42:8329, baz\nboo' \
	| ./gdprify-log \
	| grep -q 'bar host=2001:db8::ff00:42:8329, baz'

	echo -e 'foo\nbar host=2001:db8::ff00:42:8329, baz\nboo' \
	| ./gdprify-log -P \
	| grep -q 'bar host=2001:db8::, baz'

	echo -e 'foo\nbar host=10.20.5.12, baz\nboo' \
	| ./gdprify-log \
	| grep -q 'bar host=10.20.5.12, baz'

	echo -e 'foo\nbar host=10.20.5.12, baz\nboo' \
	| ./gdprify-log -p \
	| grep -q 'bar host=10.20.5.0, baz'

$(BENCHMARK_LOG): Makefile
	timeout .1 cat /dev/urandom \
	| tr -dc 'A-Za-z0-9!"#$$%&'\''()*+,-./:;<=>?@[\]^_`{|}~' \
	| fold \
	> $@

benchmark.%: $(SCRIPT) $(BENCHMARK_LOG)
	type $* \
	|| true \
	&& time -v $* -OO $^ /dev/null 2>&1 \
	| tee $@

benchmarks: $(BENCHMARKS)

profile.%: $(SCRIPT) $(BENCHMARK_LOG)
	type $* \
	|| true \
	&& $* -OOmcProfile -s ncalls $^ /dev/null 2>&1 \
	| tee $@

profiles: $(PROFILES)

clean:
	rm -f \
		$(BENCHMARK_LOG) \
		$(PROFILES) \
		$(BENCHMARKS)
