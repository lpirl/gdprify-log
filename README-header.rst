GDPRify logs
============

.. image:: https://gitlab.com/lpirl/gdprify-log/badges/master/pipeline.svg
  :target: https://gitlab.com/lpirl/gdprify-log/pipelines
  :align: right

A script to anonymize log files to, e.g., comply with the GDPR through
an `integration with logrotate`_.

Pure Python 3, no dependencies.

::
