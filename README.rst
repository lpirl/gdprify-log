
GDPRify logs
============

.. image:: https://gitlab.com/lpirl/gdprify-log/badges/master/pipeline.svg
  :target: https://gitlab.com/lpirl/gdprify-log/pipelines
  :align: right

A script to anonymize log files to, e.g., comply with the GDPR through
an `integration with logrotate`_.

Pure Python 3, no dependencies.

::

  usage: gdprify-log [-h] [-d] [-v] [--inplace] [-a ANONYMIZERS] [--ipv6mask N]
                     [--private-ipv6] [--ipv4mask N] [--private-ipv4]
                     [in_file] [out_file]
  
  This script is intended to anonymize log files. This can be useful, e.g., to
  fulfill the General Data Protection Regulation (GDPR, German: DSGVO).
  Currently, IPv4, IPv6 and email addresses are considered. False positives are
  more tolerable than false negatives. Effectiveness over efficiency. For more
  information, e.g., how to integrate with log rotation, please see
  https://gitlab.com/lpirl/gdprify-log .
  
  positional arguments:
    in_file               file to read from, stdin: - (default: -)
    out_file              file to write to, stdout: - (default: -)
  
  options:
    -h, --help            show this help message and exit
    -d, --debug           turn on debug messages (default: False)
    -v, --verbose         turn on verbose messages (default: False)
    --inplace             replace input file (out_file will be ignored)
                          (default: False)
    -a ANONYMIZERS, --anonymizers ANONYMIZERS
                          comma separated list of anonymizers to enable
                          (default:
                          Ipv6Anonymizer,Ipv4Anonymizer,EmailAnonymizer)
    --ipv6mask N          zero rightmost N bits of IPv6 addresses, 1 <= N <= 128
                          (default: 64)
    --private-ipv6, -P    zero private IPv6 addresses too (default: False)
    --ipv4mask N          zero rightmost N bits of IPv4 addresses, 1 <= N <= 32
                          (default: 8)
    --private-ipv4, -p    zero private IPv4 addresses too (default: False)

integration with logrotate
--------------------------

If you want to anonymize your logs when rotating them with `logrotate
<https://github.com/logrotate/logrotate>`__, place the following lines
in, e.g., ``/etc/logrotate.d/000-anonymize-before-compression``::

  compress
  compresscmd /bin/sh
  compressoptions -c '/where/ever/gdprify-log | gzip'

With this global setting, all logs are anonymized before compression
and it is not required to modify every logrotate rule.

credits
-------

The implementation to anonymize the rightmost bits of IP addresses has
been taken from https://www.privacyfoundation.ch/de/service/anonip.html
mainly. Big thanks.
